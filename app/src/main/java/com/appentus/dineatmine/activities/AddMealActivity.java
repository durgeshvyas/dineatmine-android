package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.adapters.AddMealsDietAdapter;
import com.appentus.dineatmine.adapters.AddMealsFitnessAdapter;

public class AddMealActivity extends AppCompatActivity {
    Activity activity = this;
    Spinner spinner,spinnernum,spdrink,spdrink2;
    RecyclerView recyclerDiet,rectclerFitness;


    String text[] ={"Kosher", "Vegan", "Halal","Nuts", "Vegan", "Halal","Nuts"};
    String text1[] ={"Veg", "Diary", "Soy","Carbs", "Diary", "Soy","Carbs"};

    String fitness[] ={"comfort", "Healthy", "skinny"};
    Integer img[] ={R.drawable.fruit,R.drawable.fruit,R.drawable.fruit};

    String[] category={"French","Indian"};
    String[] number={"1","2"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meal);

        recyclerDiet = findViewById(R.id.recycler_diet);
        rectclerFitness = findViewById(R.id.recycler_fitness);
        spinner = findViewById(R.id.spinner);
        spinnernum = findViewById(R.id.spinner1);
        spdrink = findViewById(R.id.spinner2);
        spdrink2 = findViewById(R.id.spinner3);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        recyclerDiet.setLayoutManager(linearLayoutManager);
        AddMealsDietAdapter addMealsDietAdapter = new AddMealsDietAdapter(activity, text,text1);
        recyclerDiet.setAdapter(addMealsDietAdapter);


        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        rectclerFitness.setLayoutManager(linearLayoutManager1);
        AddMealsFitnessAdapter addMealsFitnessAdapter = new AddMealsFitnessAdapter(activity, fitness,img);
        rectclerFitness.setAdapter(addMealsFitnessAdapter);



        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,category);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(aa);

        ArrayAdapter num = new ArrayAdapter(this,android.R.layout.simple_spinner_item,number);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnernum.setAdapter(num);
        spdrink.setAdapter(num);
        spdrink2.setAdapter(num);

    }
}
