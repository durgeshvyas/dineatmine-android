package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.fragments.HomePageFragment;
import com.appentus.dineatmine.fragments.InboxFragment;
import com.appentus.dineatmine.fragments.ProfileFragment;
import com.appentus.dineatmine.fragments.ReservationFragment;
import com.appentus.dineatmine.fragments.SearchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BaseActivity extends AppCompatActivity {

    Activity activity = this;
    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.tv_explore)
    TextView tvExplore;
    @BindView(R.id.llsearch)
    LinearLayout llsearch;
    @BindView(R.id.iv_saved)
    ImageView ivSaved;
    @BindView(R.id.tv_saved)
    TextView tvSaved;
    @BindView(R.id.llsaved)
    LinearLayout llsaved;
    @BindView(R.id.iv_reservation)
    ImageView ivReservation;
    @BindView(R.id.tv_reservation)
    TextView tvReservation;
    @BindView(R.id.llreservation)
    LinearLayout llreservation;
    @BindView(R.id.iv_inbox)
    ImageView ivInbox;
    @BindView(R.id.tv_inbox)
    TextView tvInbox;
    @BindView(R.id.llinbox)
    LinearLayout llinbox;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_profile)
    TextView tvProfile;
    @BindView(R.id.llprofile)
    LinearLayout llprofile;
    @BindView(R.id.mainframe)
    FrameLayout mainframe;
    @BindView(R.id.llbottom)
    LinearLayout llbottom;
    @BindView(R.id.home_page)
    ConstraintLayout homePage;
    private TextView mTextMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);
        ivSearch.setImageResource(R.drawable.search_icon);
        tvExplore.setTextColor(getResources().getColor(R.color.colorDine));
        getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new HomePageFragment()).commit();


    }

    @OnClick({R.id.llsearch, R.id.llsaved, R.id.llreservation, R.id.llinbox, R.id.llprofile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llsearch:
                ivSearch.setImageResource(R.drawable.search_icon);
                tvExplore.setTextColor(getResources().getColor(R.color.colorDine));

                getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new HomePageFragment()).commit();

                ivSaved.setImageResource(R.drawable.saved_white);
                tvSaved.setTextColor(getResources().getColor(R.color.colorWhite));
                ivReservation.setImageResource(R.drawable.reservation_white);
                tvReservation.setTextColor(getResources().getColor(R.color.colorWhite));
                ivInbox.setImageResource(R.drawable.inbox_white);
                tvInbox.setTextColor(getResources().getColor(R.color.colorWhite));
                ivProfile.setImageResource(R.drawable.profile_white);
                tvProfile.setTextColor(getResources().getColor(R.color.colorWhite));

                break;

            case R.id.llsaved:

                getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new SearchFragment()).commit();

                ivSaved.setImageResource(R.drawable.saved_pink);
                tvSaved.setTextColor(getResources().getColor(R.color.colorDine));
                ivSearch.setImageResource(R.drawable.search_white);
                tvExplore.setTextColor(getResources().getColor(R.color.colorWhite));
                ;
                ivReservation.setImageResource(R.drawable.reservation_white);
                tvReservation.setTextColor(getResources().getColor(R.color.colorWhite));
                ivInbox.setImageResource(R.drawable.inbox_white);
                tvInbox.setTextColor(getResources().getColor(R.color.colorWhite));
                ivProfile.setImageResource(R.drawable.profile_white);
                tvProfile.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.llreservation:

                getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new ReservationFragment()).commit();

                ivReservation.setImageResource(R.drawable.reservation_pink);
                tvReservation.setTextColor(getResources().getColor(R.color.colorDine));
                ivSaved.setImageResource(R.drawable.saved_white);
                tvSaved.setTextColor(getResources().getColor(R.color.colorWhite));
                ivSearch.setImageResource(R.drawable.search_white);
                tvExplore.setTextColor(getResources().getColor(R.color.colorWhite));
                ;
                ivInbox.setImageResource(R.drawable.inbox_white);
                tvInbox.setTextColor(getResources().getColor(R.color.colorWhite));
                ivProfile.setImageResource(R.drawable.profile_white);
                tvProfile.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case R.id.llinbox:


                getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new InboxFragment()).commit();

                ivInbox.setImageResource(R.drawable.inbox_icon);
                tvInbox.setTextColor(getResources().getColor(R.color.colorDine));
                ivSaved.setImageResource(R.drawable.saved_white);
                tvSaved.setTextColor(getResources().getColor(R.color.colorWhite));
                ivSearch.setImageResource(R.drawable.search_white);
                tvExplore.setTextColor(getResources().getColor(R.color.colorWhite));
                ivReservation.setImageResource(R.drawable.reservation_white);
                tvReservation.setTextColor(getResources().getColor(R.color.colorWhite));
                ivProfile.setImageResource(R.drawable.profile_white);
                tvProfile.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case R.id.llprofile:

                getSupportFragmentManager().beginTransaction().add(R.id.mainframe, new ProfileFragment()).commit();


                ivProfile.setImageResource(R.drawable.profile_pink);
                tvProfile.setTextColor(getResources().getColor(R.color.colorDine));
                ivSaved.setImageResource(R.drawable.saved_white);
                tvSaved.setTextColor(getResources().getColor(R.color.colorWhite));
                ivSearch.setImageResource(R.drawable.search_white);
                tvExplore.setTextColor(getResources().getColor(R.color.colorWhite));
                ;
                ivReservation.setImageResource(R.drawable.reservation_white);
                tvReservation.setTextColor(getResources().getColor(R.color.colorWhite));
                ivInbox.setImageResource(R.drawable.inbox_white);
                tvInbox.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
        }
    }


}
