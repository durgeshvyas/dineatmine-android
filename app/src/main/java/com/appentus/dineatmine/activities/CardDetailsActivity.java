package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.appentus.dineatmine.R;
import com.braintreepayments.cardform.view.CardForm;

public class CardDetailsActivity extends AppCompatActivity {

    Activity activity = this;
    CardForm cardForm;
    ImageView iv_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);

        cardForm = findViewById(R.id.card_form);
        iv_next = findViewById(R.id.iv_next);

        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .postalCodeRequired(true)
                .setup(activity);
    }
}
