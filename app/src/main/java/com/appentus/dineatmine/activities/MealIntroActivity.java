package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.adapters.MealsDietAdapter;
import com.appentus.dineatmine.adapters.MealsFitnessAdapter;

public class MealIntroActivity extends AppCompatActivity {
    Activity activity = this;
    RecyclerView recycler_diet,recycler_fitness,recyclerView;
    String text[] ={"Kosher", "Small", "Medium"};
    String fitness[] ={"Healthy", "Very Healthy", "Not Healthy"};
    Integer img[] ={R.drawable.fruit,R.drawable.fruit,R.drawable.fruit};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_intro);

        recycler_diet = findViewById(R.id.recycler_diet);
        recycler_fitness = findViewById(R.id.recycler_fitness);
        recyclerView = findViewById(R.id.recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        recycler_diet.setLayoutManager(linearLayoutManager);
        MealsDietAdapter mealsDietAdapter = new MealsDietAdapter(activity, text);
        recycler_diet.setAdapter(mealsDietAdapter);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        recycler_fitness.setLayoutManager(linearLayoutManager1);
        MealsFitnessAdapter mealsFitnessAdapter = new MealsFitnessAdapter(activity, fitness,img);
        recycler_fitness.setAdapter(mealsFitnessAdapter);


        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager2);
        MealsFitnessAdapter mealsFitnessAdapter1 = new MealsFitnessAdapter(activity, fitness,img);
        recyclerView.setAdapter(mealsFitnessAdapter1);

    }
}
