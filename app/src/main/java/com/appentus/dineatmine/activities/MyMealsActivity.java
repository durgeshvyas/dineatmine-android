package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.adapters.MealsAdapter;

public class MyMealsActivity extends AppCompatActivity {
    Activity activity = this;
    RecyclerView recyclerView;
    String text[] ={"Large Size Salad", "Small Size Salad", "Medium Size Salad","Chikan Salad","Large Chikan Salad","Green Salad"};
    String subtext[] ={"\u00a3 8.75", "\u00a3 4.75", "\u00a3 5.75","\u00a3 7.75","\u00a3 6.75","\u00a3 9.75"};
    Integer img[] ={R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon,
            R.drawable.mealicon,R.drawable.mealicon};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_meals);

        recyclerView = findViewById(R.id.recycler);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        MealsAdapter mealsAdapter = new MealsAdapter(activity, text,subtext,img);
        recyclerView.setAdapter(mealsAdapter);

    }
}
