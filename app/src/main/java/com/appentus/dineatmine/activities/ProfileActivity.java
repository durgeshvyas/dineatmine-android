package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.appentus.dineatmine.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {
    Activity activity = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.tv_switchguest, R.id.iv_next2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_switchguest:

                startActivity(new Intent(activity,AddMealActivity.class));

                break;
            case R.id.iv_next2:

                startActivity(new Intent(activity,AddMealActivity.class));

                break;
        }
    }
}
