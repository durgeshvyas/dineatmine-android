package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appentus.dineatmine.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileSettingActivity extends AppCompatActivity {
    Activity activity = this;
    @BindView(R.id.iv_Back)
    ImageView ivBack;
    @BindView(R.id.tvResversation)
    TextView tvResversation;
    @BindView(R.id.ivNext)
    ImageView ivNext;
    @BindView(R.id.tvPayment)
    TextView tvPayment;
    @BindView(R.id.ivNext1)
    ImageView ivNext1;
    @BindView(R.id.tvCurrency)
    TextView tvCurrency;
    @BindView(R.id.ivNext2)
    ImageView ivNext2;
    @BindView(R.id.tvTermsService)
    TextView tvTermsService;
    @BindView(R.id.ivNext3)
    ImageView ivNext3;
    @BindView(R.id.tvCalendar)
    TextView tvCalendar;
    @BindView(R.id.ivNext4)
    ImageView ivNext4;
    @BindView(R.id.tvLogout)
    TextView tvLogout;
    @BindView(R.id.ivNext5)
    ImageView ivNext5;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.scrollView)
    ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_Back, R.id.tvResversation, R.id.ivNext, R.id.tvPayment, R.id.ivNext1, R.id.tvCurrency, R.id.ivNext2, R.id.tvTermsService, R.id.ivNext3, R.id.tvCalendar, R.id.ivNext4, R.id.tvLogout, R.id.ivNext5})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_Back:
                break;
            case R.id.tvResversation:
                break;
            case R.id.ivNext:
                break;
            case R.id.tvPayment:
                startActivity(new Intent(activity,PaymentMethodActivity.class));
                break;
            case R.id.ivNext1:
                startActivity(new Intent(activity,PaymentMethodActivity.class));
                break;
            case R.id.tvCurrency:
                break;
            case R.id.ivNext2:
                break;
            case R.id.tvTermsService:
                break;
            case R.id.ivNext3:
                break;
            case R.id.tvCalendar:
                break;
            case R.id.ivNext4:
                break;
            case R.id.tvLogout:
                break;
            case R.id.ivNext5:
                break;
        }
    }
}
