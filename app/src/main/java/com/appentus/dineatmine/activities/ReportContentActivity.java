package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.appentus.dineatmine.R;

public class ReportContentActivity extends AppCompatActivity {
Activity activity =this;
ImageView ib_report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_content);

        ib_report =  findViewById(R.id.btn_report);

        ib_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity,RequestActivity.class));
            }
        });
   }
}
