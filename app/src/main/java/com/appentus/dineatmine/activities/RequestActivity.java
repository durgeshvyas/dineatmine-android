package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.appentus.dineatmine.R;

public class RequestActivity extends AppCompatActivity {
Activity activity = this;
ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        imageView = findViewById(R.id.iv_profile);
imageView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(activity,ReservingActivity.class));
    }
});

    }
}
