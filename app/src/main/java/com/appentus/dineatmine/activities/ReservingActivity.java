package com.appentus.dineatmine.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.appentus.dineatmine.R;

public class ReservingActivity extends AppCompatActivity {
    Activity activity = this;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmentreserving);
        btn = findViewById(R.id.btSubmit);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity,ExploringActivity.class));
            }
        });
    }
}
