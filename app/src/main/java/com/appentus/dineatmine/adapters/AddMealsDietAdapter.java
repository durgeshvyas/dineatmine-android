package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.utils.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMealsDietAdapter extends RecyclerView.Adapter<AddMealsDietAdapter.ViewHolder> {
    Activity activity;
    String text[];
    String text1[];

    public AddMealsDietAdapter(Activity activity, String text[], String text1[]) {
        this.activity = activity;
        this.text = text;
        this.text1 = text1;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = activity.getLayoutInflater().inflate(R.layout.item_addmeal_diet_intro, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvDietname.setText(text[position]);
        holder.llCardView.setBackgroundDrawable(new CardView(activity.getResources().getColor(R.color.colorPrimary), new float[]{100, 100, 100, 100, 100, 100, 100, 100}));

        holder.tvDietname1.setText(text1[position]);
        holder.llCardView1.setBackgroundDrawable(new CardView(activity.getResources().getColor(R.color.colorPrimary), new float[]{100, 100, 100, 100, 100, 100, 100, 100}));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return text.length;
    }

    static
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_dietname)
        TextView tvDietname;
        @BindView(R.id.llCardView)
        LinearLayout llCardView;
        @BindView(R.id.checkbox)
        CheckBox checkbox;
        @BindView(R.id.tv_dietname1)
        TextView tvDietname1;
        @BindView(R.id.llCardView1)
        LinearLayout llCardView1;
        @BindView(R.id.checkbox1)
        CheckBox checkbox1;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

}
