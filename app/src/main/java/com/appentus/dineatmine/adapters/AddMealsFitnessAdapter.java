package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.appentus.dineatmine.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMealsFitnessAdapter extends RecyclerView.Adapter<AddMealsFitnessAdapter.ViewHolder> {
    Activity activity;
    String fitness[];
    Integer img[];


    public AddMealsFitnessAdapter(Activity activity, String fitness[], Integer img[]) {
        this.activity = activity;
        this.fitness = fitness;
        this.img = img;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = activity.getLayoutInflater().inflate(R.layout.item_addmeal_fitness_intro, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvFruitname.setText(fitness[position]);
        holder.ivFruit.setImageResource(img[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return fitness.length;
    }

    static
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_fruit)
        ImageView ivFruit;
        @BindView(R.id.tv_fruitname)
        TextView tvFruitname;
        @BindView(R.id.checkbox)
        CheckBox checkbox;
        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
