package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.activities.ProfileActivity;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder>
{
    Activity activity;
    String text[];
    String desp[];
    String miles[];
    String subtext[];
    Integer img[];

    public HomeAdapter(Activity activity, String text[],String desp[],String miles[], String subtext[], Integer img[])
    {
        this.activity = activity;
        this.text = text;
        this.desp = desp;
        this.miles = miles;
        this.subtext = subtext;
        this.img = img;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = activity.getLayoutInflater().inflate(R.layout.item_home_layout,parent,false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.iv_dishProfile.setImageResource(img[position]);
        holder.tv_Dishname.setText(text[position]);
        holder.tv_Price.setText(subtext[position]);
        holder.tv_desp.setText(desp[position]);
        holder.tv_miles.setText(miles[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return text.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_Dishname,tv_Price,tv_desp,tv_miles;
        ImageView iv_dishProfile;

        public ViewHolder(View itemView)
        {
            super(itemView);

            tv_Dishname = itemView.findViewById(R.id.tv_item_title);
            tv_Price = itemView.findViewById(R.id.tv_price);
            tv_desp = itemView.findViewById(R.id.tv_item_desp);
            tv_miles = itemView.findViewById(R.id.tv_item_miles);
            iv_dishProfile = itemView.findViewById(R.id.iv_profile);

        }
    }

}
