package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.activities.ProfileActivity;

public class MealsAdapter extends RecyclerView.Adapter<MealsAdapter.ViewHolder>
{
    Activity activity;
    String text[];
    String subtext[];
    Integer img[];

    public MealsAdapter(Activity activity,String text[],String subtext[],Integer img[])
    {
        this.activity = activity;
        this.text = text;
        this.subtext = subtext;
        this.img = img;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = activity.getLayoutInflater().inflate(R.layout.my_meal_item_layout,parent,false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.iv_dishProfile.setImageResource(img[position]);
        holder.tv_Dishname.setText(text[position]);
        holder.tv_Price.setText(subtext[position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return text.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_Dishname,tv_Price;
        ImageView iv_dishProfile;

        public ViewHolder(View itemView)
        {
            super(itemView);

            tv_Dishname = itemView.findViewById(R.id.tv_item_title);
            tv_Price = itemView.findViewById(R.id.tv_price);
            iv_dishProfile = itemView.findViewById(R.id.iv_profile);

        }
    }

}
