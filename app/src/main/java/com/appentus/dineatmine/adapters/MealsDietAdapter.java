package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.utils.CardView;

public class MealsDietAdapter extends RecyclerView.Adapter<MealsDietAdapter.ViewHolder>
{
    Activity activity;
    String text[];

    public MealsDietAdapter(Activity activity, String text[])
    {
        this.activity = activity;
        this.text = text;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = activity.getLayoutInflater().inflate(R.layout.item_meal_diet_intro,parent,false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tv_Dishname.setText(text[position]);
        holder.llCardView.setBackgroundDrawable(new CardView(activity.getResources().getColor(R.color.colorPrimary), new float[]{100, 100, 100, 100, 100, 100, 100, 100}));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return text.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_Dishname;

        LinearLayout llCardView;

        public ViewHolder(View itemView)
        {
            super(itemView);
            llCardView = itemView.findViewById(R.id.llCardView);

            tv_Dishname = itemView.findViewById(R.id.tv_dietname);


        }
    }

}
