package com.appentus.dineatmine.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appentus.dineatmine.R;

public class MealsFitnessAdapter extends RecyclerView.Adapter<MealsFitnessAdapter.ViewHolder>
{
    Activity activity;
    String fitness[];
    Integer img[];


    public MealsFitnessAdapter(Activity activity, String fitness[],Integer img[])
    {
        this.activity = activity;
        this.fitness = fitness;
        this.img = img;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = activity.getLayoutInflater().inflate(R.layout.item_meal_fitness_intro,parent,false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tv_fitness.setText(fitness[position]);
        holder.iv_fitness.setImageResource(img[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // activity.startActivity(new Intent(activity, ProfileActivity.class));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return fitness.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_fitness;
        ImageView iv_fitness;

        public ViewHolder(View itemView)
        {
            super(itemView);

            tv_fitness = itemView.findViewById(R.id.tv_fruitname);
            iv_fitness = itemView.findViewById(R.id.iv_fruit);


        }
    }

}
