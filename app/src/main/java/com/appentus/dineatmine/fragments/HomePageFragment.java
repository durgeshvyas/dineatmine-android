package com.appentus.dineatmine.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.adapters.HomeAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomePageFragment extends Fragment {


    @BindView(R.id.ed_search)
    EditText edSearch;
     @BindView(R.id.recycler)
    RecyclerView recycler;
    Unbinder unbinder;

    String text[] ={"Large Size Salad", "Small Size Salad", "Medium Size Salad","Large Size Salad", "Small Size Salad", "Medium Size Salad"};
    String desp[] = {"Carol","Marc","Tom","Carol","Marc","Tom"};
    String miles[] = {"0.3 miles","0.35miles","0.4miles","0.3 miles","0.35miles","0.4miles"};
    String subtext[] ={"\u00a3 8.75", "\u00a3 4.75", "\u00a3 5.75","\u00a3 8.75", "\u00a3 4.75", "\u00a3 5.75"};
    Integer img[] ={R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon,R.drawable.mealicon};


    public HomePageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);
        unbinder = ButterKnife.bind(this, view);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3);
        recycler.setLayoutManager(gridLayoutManager);
        HomeAdapter homeAdapter = new HomeAdapter(getActivity(), text,desp,miles,subtext,img);
        recycler.setAdapter(homeAdapter);



        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
