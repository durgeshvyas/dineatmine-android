package com.appentus.dineatmine.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appentus.dineatmine.R;
import com.appentus.dineatmine.activities.AddMealActivity;
import com.appentus.dineatmine.activities.ProfileSettingActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    @BindView(R.id.tv_profile)
    TextView tvProfile;
    @BindView(R.id.iv_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_profile_name)
    TextView tvProfileName;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.viewandedit)
    TextView viewandedit;
    @BindView(R.id.iv_next)
    ImageView ivNext;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.listmenu)
    TextView listmenu;
    @BindView(R.id.iv_add)
    ImageView ivAdd;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.tv_settings)
    TextView tvSettings;
    @BindView(R.id.iv_next1)
    ImageView ivNext1;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.tv_switchguest)
    TextView tvSwitchguest;
    @BindView(R.id.iv_next2)
    ImageView ivNext2;
    @BindView(R.id.view4)
    View view4;
    @BindView(R.id.view5)
    View view5;
    @BindView(R.id.tv_othehost)
    TextView tvOthehost;
    @BindView(R.id.view6)
    View view6;
    @BindView(R.id.tv_notification)
    TextView tvNotification;
    @BindView(R.id.iv_next3)
    ImageView ivNext3;
    @BindView(R.id.view7)
    View view7;
    @BindView(R.id.tv_needhelp)
    TextView tvNeedhelp;
    @BindView(R.id.iv_next4)
    ImageView ivNext4;
    @BindView(R.id.view8)
    View view8;
    @BindView(R.id.tv_feedback)
    TextView tvFeedback;
    @BindView(R.id.iv_next5)
    ImageView ivNext5;
    @BindView(R.id.view9)
    View view9;
    @BindView(R.id.tv_refer)
    TextView tvRefer;
    @BindView(R.id.iv_next6)
    ImageView ivNext6;
    @BindView(R.id.view10)
    View view10;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    Unbinder unbinder;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_profile, R.id.tv_profile_name, R.id.viewandedit, R.id.iv_next, R.id.listmenu, R.id.iv_add, R.id.tv_settings, R.id.iv_next1, R.id.tv_switchguest, R.id.iv_next2, R.id.tv_notification, R.id.iv_next3, R.id.tv_needhelp, R.id.iv_next4, R.id.tv_feedback, R.id.iv_next5})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_profile:
                break;
            case R.id.tv_profile_name:
                break;
            case R.id.viewandedit:
                break;
            case R.id.iv_next:
                break;
            case R.id.listmenu:
                getActivity().startActivity(new Intent(getActivity(), AddMealActivity.class));
                break;
            case R.id.iv_add:
                getActivity().startActivity(new Intent(getActivity(), AddMealActivity.class));
                break;
            case R.id.tv_settings:
                getActivity().startActivity(new Intent(getActivity(), ProfileSettingActivity.class));
                break;
            case R.id.iv_next1:
                getActivity().startActivity(new Intent(getActivity(), ProfileSettingActivity.class));
                break;
            case R.id.tv_switchguest:
                break;
            case R.id.iv_next2:
                break;
            case R.id.tv_notification:
                break;
            case R.id.iv_next3:
                break;
            case R.id.tv_needhelp:
                break;
            case R.id.iv_next4:
                break;
            case R.id.tv_feedback:
                break;
            case R.id.iv_next5:
                break;
        }
    }
}
