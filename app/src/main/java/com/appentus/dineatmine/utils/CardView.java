package com.appentus.dineatmine.utils;

import android.graphics.drawable.GradientDrawable;

public class CardView extends GradientDrawable {
    public CardView(int pStartColor,float[] radius) {
        super(Orientation.BOTTOM_TOP, new int[]{pStartColor, pStartColor, pStartColor});
        setShape(GradientDrawable.RECTANGLE);

       /* float[] radius = {mTopLeftRadius, mTopLeftRadius, mTopRightRadius, mTopRightRadius, mBottomRightRadius,
                mBottomRightRadius, mBottomLeftRadius, mBottomLeftRadius};*/

        setCornerRadii(radius);

    }
}
