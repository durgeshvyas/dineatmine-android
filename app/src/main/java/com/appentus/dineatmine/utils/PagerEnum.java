package com.appentus.dineatmine.utils;

import com.appentus.dineatmine.R;

public enum PagerEnum {
    ONE(R.layout.slider_layout_one),
    TWO(R.layout.slider_layout_two),
    THREE(R.layout.slider_layout_three);

    private int mLayoutResId;

    PagerEnum(int layoutResId) {
        mLayoutResId = layoutResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
