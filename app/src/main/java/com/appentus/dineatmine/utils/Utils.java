package com.appentus.dineatmine.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;

public class Utils {


    public static void startActivity(Activity activity, Intent intent, boolean finish) {
        activity.startActivity(intent);
        if (finish) {
            activity.finish();
        }
    }

    public static void startActivity(Activity activity, Intent intent) {
        activity.startActivity(intent);
    }


    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void changeStatusBarColor(Activity con) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = con.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

}
